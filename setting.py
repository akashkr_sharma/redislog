import env

class ENVSetting:

	def __init__(self):
		self.uname = self.password = self.host = self.dbname = ''

	def getPGSetting(self):
		self.uname = getattr(env,env.ENV+'_pg_uname')
		self.password = getattr(env,env.ENV+'_pg_password')
		self.host = getattr(env,env.ENV+'_pg_host')
		self.dbname = getattr(env,env.ENV+'_pg_database')


	def createConnetionLink(self):
		self.getPGSetting()
		return 'postgresql://'+self.uname+':'+self.password+'@'+self.host+'/'+self.dbname