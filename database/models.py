from setting import ENVSetting
import importlib
from sqlalchemy.orm import session
from sqlalchemy.orm import  sessionmaker,session
from sqlalchemy import create_engine
from rq import Queue
from redis import Redis


session = session.Session(bind=create_engine(ENVSetting().createConnetionLink()))

def store(modelName, values):
	module = importlib.import_module('database.tables.'+modelName+'Table')
	modelName = getattr(module, modelName)
	md = modelName(**values)
	session.add(md)
	session.commit()


def get(modelName, values=None):
	module = importlib.import_module('database.tables.'+modelName+'Table')
	modelName = getattr(module, modelName)
	data = modelName.query.all() if values==None else modelName.query.get(values).transform()
	return data
	# transformedFilterData = []
	# if data:
	# 	for i in data:
	# 		transformedFilterData.append(i.transform())
	# return transformedFilterData


def filter(modelName, values):
	module = importlib.import_module('database.tables.'+modelName+'Table')
	modelName = getattr(module, modelName)
	filterData = modelName.query.filter_by(**values).all()
	transformedFilterData = []
	if data:
		for i in data:
			transformedFilterData.append(i.transform())
	return transformedFilterData





class RedisDB:
	def __init__(self, host, port=None):
		self.redis_conn = Redis(host=host, port=int(port) if port else 6379, socket_connect_timeout=2, socket_timeout=2)
		self.queueExecute = Queue('high', connection=self.redis_conn)

	def execute(self, queueFunc, data, *args):
		job = self.queueExecute.enqueue_call(queueFunc, args=(data, args))

	def redisKeys(self, pattern='*'):
		return self.redis_conn.keys(pattern)

	def llenKey(self, key):
		return self.redis_conn.llen(key)

	def lselect(self, name, startR=0, stop=-1):
		return self.redis_conn.lrange(name , startR, stop)
	
	def redisType(self, key):
		return self.redis_conn.type(key)
	# def selectKeys(self, pattern='*'):
	# 	return self.redis_conn.keys('*' if pattern == '*' or pattern == '' else pattern if pattern =='completed' or pattern=='failed' else 'queue*' )