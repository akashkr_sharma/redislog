from config import db
from sqlalchemy.sql import func
from datetime import datetime


class RedisServer(db.Model):
	__tablename__ = 'redis_server'
	id =  db.Column(db.String, primary_key=True)
	name = db.Column(db.String, unique=True, nullable=False)
	ip_address = db.Column(db.String, unique=True, nullable=False)
	port = db.Column(db.String, nullable=True)
	created_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
	updated_at = db.Column(db.DateTime, nullable=True, onupdate=func.now())
	# last_login = db.Column(db.DateTime, nullable=False, server_default=func.now())
	# deleted = db.Column(db.Boolean(), default=False

	def transform(self):
		return {
			'id' : self.id,
			'ipAddress' : self.ip_address,
			'port' : self.port,
			'createdAt' : self.created_at,
			'updatedAt' : self.updated_at,
			# 'lastLoginAt' : self.last_login,
			# 'deleted' : self.deleted
		}
