from database.models import *

class DB():
	
	def create(self, model, value):
		return store(model, value)

	def get(self, model, value=None):
		return get(model, value)

	def filter(self, model, value={}):
		return filter(model, value)

# redisConn = RedisDB().