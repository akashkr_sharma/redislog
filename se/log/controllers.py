from helpers import *
from models import DB
from database import models as dbm
from flask import session
from redis import StrictRedis, Redis
from json import dumps, loads
# from rq import Queue

lm = DB()

def getServer():
	data = lm.get(pg_server_db)
	return transform(data)

def regConnection(id):
	data = lm.get(pg_server_db, id)
	try:
		print data
		# data['port'] = 5000
		if(data['ipAddress'] == 'localhost' or data['ipAddress'] == '127.0.0.1'):
			try : 
				if data['port'] != '' and int(data['port']) == 5000:
					# print "2"
					raise Exception('%s port is not allowed for localhost!!!'%data['port']) 
			except Exception as e:
				raise e
		conn = Redis(host=data['ipAddress'], port= int(data['port']) if data['port'] else 6379, socket_connect_timeout=2, socket_timeout=2)
		conn.ping()
		session['conn_id'] = id
	except Exception as e:
		raise Exception({
				"1)" : "checkout port",
				"2)" : e.message
			})