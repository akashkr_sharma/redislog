from views import Log, GetLog

urls = [
	('/', ['GET'], GetLog.as_view('getlog')),
	('/log/<id>', ['GET'], Log.as_view('log')),
	('/logout', ['GET'], Log.as_view('logout'))
]