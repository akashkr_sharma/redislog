from flask import jsonify, request, session
from flask.views import MethodView
from controllers import *
from seResponse.SeResponse import SeResponse
import traceback, sys

class Log(MethodView):
	def get(self, id=None):
		# try:
			# if((session['conn'] != None or session['conn'] == undefined ))
				# raise Exception('you r connected with other server please log first then login')
		# create connection for special server request
		try:
			if id:
				regConnection(id)
				return SeResponse('200', hint='Congrats u r logged in').seSuccess()
			else : 
				session.clear()
				return SeResponse('200', hint='u r logout').seSuccess()
		except Exception as e :
			traceback.print_exc(file=sys.stdout)
			return SeResponse('500', hint=e).seError()


class GetLog(MethodView):
	def get(self):
		try :
			return SeResponse('200', getServer()).seSuccess()
		except Exception as e:
			traceback.print_exc(file=sys.stdout)
			return SeResponse('500', hint=str(e)).seError()