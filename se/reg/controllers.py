from models import DB
import uuid

modeldb = DB()

def registerServer(pg_server_db, request_data):
	print request_data
	transformed_data = {
		'id' : uuid.uuid4(),
		'name' : request_data['name'],
		'ip_address' : request_data['serverip'],
		'port' : request_data['port']
	}
	return modeldb.create(pg_server_db, transformed_data)