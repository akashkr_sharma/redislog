from flask import jsonify, request
from flask.views import MethodView
from seResponse.SeResponse import SeResponse
from controllers import *
from helpers import *

class Reg(MethodView):
	def post(self):
		try:
			rd = request.get_json()
			print rd
			request_data = {
				'serverip' : rd['ipAddress'],
				'port' : rd['port'],
				'name' : rd['name']
			}
			return SeResponse('201', registerServer(pg_server_db, request_data)).seSuccess()
		except Exception as e : 
			return SeResponse('500', hint=str(e)).seError()
