from views import Keys, KeysValue

urls = [
	('/keys', ['GET'], Keys.as_view('keys')),
	('/keys/<keys>', ['GET'], KeysValue.as_view('keys_value'))
]