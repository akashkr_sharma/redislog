from database.models import *
from flask import session
from helpers import pg_server_db

class DB():
	
	def create(self, model, value):
		return store(model, value)

	def get(self, model, value=None):
		return get(model, value)

	def filter(self, model, value={}):
		return filter(model, value)

lm = DB()

def sessionCall():
	try:
		id = session['conn_id']
		data = lm.get(pg_server_db, id)
		# print keyListval
		return data
	except Exception as e:
		raise Exception('Connection Error. help: '+e.message)

def redisConn(data):
	conn = RedisDB(data['ipAddress'], data['port'])
	# print conn.redis_conn
	conn.redis_conn.ping()
	return conn