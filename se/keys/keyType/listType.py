from se.keys.models import sessionCall, redisConn
from flask import session, request
import json
from phpserialize import *


data = sessionCall()
conn = redisConn(data)

def serialise(redisKey):
	final_list = []
	try:
		items = request.args.get('items')
		page = request.args.get('page')
	finally:
		items = page = 1
	serialize_data = conn.lselect(redisKey, int(items)-1, int(items)-1)
	if conn.redis_conn.llen(redisKey):
		for i in range(len(serialize_data)):
			final_dict = {}
			json_load = json.loads(serialize_data[i])
			for key in json_load:
				key = key.encode('utf-8')
				if type(json_load[key]) is dict:
					final_dict[key] = dictObject(json_load[key], key)
				if type(json_load[key]) is unicode:
					final_dict[key] = judgeUnicode(json_load[key], key)
				if type(json_load[key]) is int:
					final_dict[key] = json_load[key]
			final_list.append(final_dict)
	return final_list

def dictObject(dict_data, l):
	dict_all = {}
	for key in dict_data:
		key1 = key
		if type(key) is unicode:
			key1 = key.encode('utf-8')
			if '*' in key:
				key1 = key1.split('*')[-1]
				key1 = key1[1:].encode('utf-8')
		if type(dict_data[key]) is unicode:
			dict_all[key1] = judgeUnicode(dict_data[key], key)
		if type(dict_data[key]) is phpobject:
			dict_all[key1] = dictObject(dict_data[key]._asdict(), key)
		if type(dict_data[key]) is dict:
			dict_all[key1] = dictObject(dict_data[key], key)
		if type(dict_data[key]) is None or type(dict_data[key]) is bool or type(dict_data[key]) is int:
			dict_all[key1] = dict_data[key]
	return dict_all

def judgeUnicode(unicode_data, key):
	try:
		return unserialise(unicode_data, key)
	except:
		try:
			""" 
				still I'm not merging the unicode check with str check so don't remove
				str check condition for unicode.
			 """
			while type(unicode_data) is unicode:
				unicode_data = json.loads(unicode_data)

			return unicode_data
			
		except:
			if unicode_data.encode('utf-8') is '':
				return 
		if type(unicode_data.encode('utf-8')) is str:
			return unicode_data.encode('utf-8')

def unserialise( serialised_data, key):
	php_object = loads(serialised_data, object_hook = phpobject)
	php_into_dict = php_object._asdict()
	return dictObject(php_into_dict,key)

def totalElement(redisKey):
	return conn.redis_conn.llen(redisKey)