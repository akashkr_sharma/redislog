pg_server_db = 'RedisServer'

def listType(key):
	try:
		# module = import_module('keyType.'+_getframe().f_code.co_name+'.serialise')
		from keyType.listType import serialise, totalElement
		return {
			'data' : serialise(key),
			'total' : totalElement(key)
		}
	except Exception as e:
		raise e

def setType(key):
	try:
		# from keyType.setType import *
		return {
			# 'data' : serialise(key),
			# 'total' : totalMember(key)
		}
	except Exception as e:
		raise e

def hashType(key):
	print key
	pass

def transform(data):
	return {
		'id' : data['id'],
		'ipAddress' : data['ipAddress'],
		'port' : data['port']
	}
