from models import *
from database.models import *
from flask import session
from helpers import pg_server_db, listType, setType, hashType, transform

# lm = DB()

# def sessionCall():
# 	id = session['conn_id']
# 	data = lm.get(pg_server_db, id)
# 	# print keyListval
# 	return data

# def redisConn(data):
# 	conn = RedisDB(data['ipAddress'], data['port'])
# 	# print conn.redis_conn
# 	conn.redis_conn.ping()
# 	return conn

def keyList():
	try:
		data = sessionCall()
		conn = redisConn(data)
		return conn.redisKeys('*')
	except Exception as e:
		raise e
		# if e.message == 'conn_id':
		# 	raise Exception('Please Login To The Server')
		# else:
			# raise e


def redisDataByKey(keyType, key):
	return {
		'list' : listType,
		'set' : setType
	}[keyType](key)


def specialKey(key):
	try:
		data = sessionCall()
		conn = redisConn(data)
		redis_type = conn.redisType(key)
		# print redis_type
		return redisDataByKey(redis_type, key)
	except Exception as e:
		raise e
		# if e.message == 'conn_id':
		# 	raise Exception('Please Login To The Server')
		# else:
		# 	raise e

