from flask.views import MethodView
from controllers import *
from seResponse.SeResponse import SeResponse
import traceback, sys

class Keys(MethodView):

	def get(self):
		try:
			return SeResponse('200', data=keyList()).seSuccess()
		except Exception as e:
			traceback.print_exc(file=sys.stdout)
			return SeResponse('500', hint=e).seError()


class KeysValue(MethodView):

	def get(self, keys):
		try:
			return SeResponse('200', data=specialKey(keys)).seSuccess()
		except Exception as e:
			traceback.print_exc(file=sys.stdout)
			return SeResponse('500', hint=e).seError()