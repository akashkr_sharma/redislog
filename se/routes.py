# import log, reg
from log import routes as l
from reg import routes as r
from keys import routes as k

# print log.routes.urls
url_list = [
	l.urls,
	r.urls,
	k.urls
]

urls = []
for xurl in url_list:
	for url in xurl:
		urls.append(url)