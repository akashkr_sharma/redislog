#!/usr/bin/python
import os, sys

dir_path = os.path.dirname(os.path.realpath(__file__))
activate_this = dir_path+'/queueFlask/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(0,dir_path+"/queueFlask")

from __init__ import app as application
application.secret_key = 'ytfvbnjhgfdrdfyhbjnk'
