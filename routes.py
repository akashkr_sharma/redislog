from flask import Flask, session
from se.routes import urls
from flask_cors import CORS
from datetime import timedelta
from seResponse.SeResponse import SeResponse

app = Flask(__name__)
CORS(app)
app.debug = True
app.config['SECRET_KEY'] = 'inkeuhewfbfvhbnkadfjfhkjw'


# @app.before_request
# def make_session_permanent():
# 	session.permanent = True
# 	app.permanent_session_lifetime = timedelta(minutes=5)


for url in urls:
	app.add_url_rule(url[0], methods = url[1], view_func = url[2])


@app.errorhandler(405)
def MethodNotFound(error):
	return SeResponse(405, hint='Method Not Allowed. check your request').seError()

@app.errorhandler(404)
def NotFoundException(error):
	return SeResponse(404, hint='Not found. check your request').seError()

# @app.errorhandler(400)
# def BadRequestException(error):
# 	return SeResponse(400, hint='Bad Request. check your request params').seError()

# @app.errorhandler(500)
# def InternalServerError(error):
# 	return SeResponse(500, hint='Something Went Wrong'+str(error.message)).seError()

# @app.errorhandler(SeException)
# def customException(error):
# 	return error.throw_se_exception()


if __name__=='__main__':
	app.run(debug=True)